\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {Classes}
\newcommand{\laTitle}       {CS 200 Lab}
\newcounter{question}

\renewcommand{\chaptername}{Topic}

\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}

\title{}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ / \laTitle}

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

\section*{Lab instructions}

\paragraph{Turn in:}

\begin{itemize}
	\item	Once done, upload the .cpp file(s) and .hpp or .h file(s) that you worked on.
	\item	Don't zip the source files.
	\item	Don't zip the entire folder and upload that. I only want source files.
\end{itemize}

\section*{\laTopic\ lab}

	\paragraph{One project, multiple files:}
	This lab is about practicing writing structs and classes. You can
	start off with one \textbf{main.cpp} file and add new .hpp/.cpp files
	to the same project for each class, writing the testing code in main.cpp.
	
	~\\ Example starter main.cpp:
		
\begin{lstlisting}[style=code]
#include <iostream>
using namespace std;

int main()
{
	cout << "Run program #: ";
	int program;
	cin >> program;
	
	if ( program == 1 )
	{
	}
	else if ( program == 2 )
	{
	}
	// etc

	return 0;
}
\end{lstlisting}
	
	
	\paragraph{.h/.hpp files:}
	Remember to use \#ifndef / \#define commands in your header files
	to prevent the compiler from copying the same code multiple times.
	
\begin{lstlisting}[style=code]
#ifndef _UNIQUE_NAME
#define _UNIQUE_NAME

class ClassDeclaration
{
	public:
	private:
}; // must have semicolon

#endif
\end{lstlisting}

	\paragraph{.cpp files for classes?}
	If you want to, you can define your member functions \textit{within}
	the class declaration in the .hpp file. Or if you want to follow
	C++ standards, you can create a .hpp and a .cpp file for each class,
	with the member function definitions going in the .cpp file.
	
	\paragraph{Includes}
	
	When including a library from the C++ standard library, it will be in this format:
	\begin{center}
		\texttt{ \#include <iostream> }
	\end{center}
	
	When including your own .hpp or .h files, it will look like this:
	\begin{center}
		\texttt{ \#include "Recipe.hpp" }
	\end{center}
	
	If a file uses strings, make sure to \texttt{ \#include <strings> }.
	If a file uses couts/cins, make sure to \texttt{ \#include <iostream> }.
	

	\newpage
	\subsection*{Class 1: Die}
	
	\begin{center}
		\begin{tabular}{c c c c c c}
			\includegraphics{images/d4.png} &
			\includegraphics{images/d6.png} &
			\includegraphics{images/d8.png} &
			\includegraphics{images/d10.png} &
			\includegraphics{images/d12.png} &
			\includegraphics{images/d20.png} 
			\\
			d4 & d6 & d8 & d10 & d12 & d20
		\end{tabular}
	\end{center}
	
	Create a new file called \textbf{Die.hpp} or \textbf{Die.h}.
	You will declare a \textbf{struct} here that has an integer
	to track how many sides this die has, and also a function to
	roll this die. It also has two constructors to initialize the die.
	
\begin{lstlisting}[style=code]
#ifndef _DIE_HPP
#define _DIE_HPP

#include <cstdlib> // for rand()

struct Die
{
	// Constructors
	Die();
	Die( int sideCount );
	
	// Method
	int Roll();
	
	// Variable
	int sides;
};

#endif
\end{lstlisting}

	\paragraph{Default constructor:}
	In the default constructor, we will set the default amount of
	sides for the die to 6 - just a standard playing die.

\begin{lstlisting}[style=code]
Die::Die()
{
	sides = 6;
}
\end{lstlisting}

	\newpage
	\paragraph{Parameterized constructor:} 
	In the parameterized constructor, we will assign the \texttt{sides}
	member variable to whatever \texttt{sideCount} is passed in.
	
\begin{lstlisting}[style=code]
Die::Die( int sideCount )
{
	sides = sideCount;
}
\end{lstlisting}

	\paragraph{Roll:}
	Then with the Roll method, we want to generate a random number 
	based on the number of sides. ~\\
	
	This statement:
	
	\begin{center}
		\texttt{rand() \% n}
	\end{center}
	
	will give us a random number between 0 and $n-1$. For most of
	our die, we will assume that the valid numbers are 1 to \texttt{sides}
	(e.g., a 6-sided die will have 1, 2, 3, 4, 5, 6 as potential outcomes).
	
	We can offset the random number by adding a +1 at the end...
	
\begin{lstlisting}[style=code]
int Die::Roll()
{
	int random = rand() % sides + 1;
	return random;
}
\end{lstlisting}

	However, with RPG dice, the d10 actually has 0 to 9, so we could
	add in a special case to our Roll function specifically for a 10-sided die...
	
\begin{lstlisting}[style=code]
int Die::Roll()
{
	if ( sides == 10 )
	{
		return rand() % sides;
	}
	else
	{
		return rand() % sides + 1;
	}
}
\end{lstlisting}

	\newpage
	\paragraph{Example dice program:}
	In main(), you can write your own little dice rolling program.
	If you aren't sure what to write, here's an example program you can use:
	\footnote{I'm not adding any modifiers in this example program because
	I don't want to confuse people who aren't familiar with D\&D.}
	
\begin{lstlisting}[style=code]
Die d20( 20 );
int hitRoll = d20.Roll();

Die d8( 8 );
int damageRoll = d8.Roll();

cout << hitRoll << " to hit, for " 
	 << damageRoll << " damage." << endl;
\end{lstlisting}
	
\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
    
		Other dice program ideas...
		
		\begin{itemize}
			\item	Roll the same die several times and calculate the average roll value.
			\item	Roll the same die several times and add up the sum of the values.
			\item	Roll two d20 and display the lower or higher of the two values.
		\end{itemize}
		
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
		\includegraphics[width=6cm]{images/sprites-together-4x.png} ~\\
		My D\&D party
    \end{subfigure}
\end{figure}
	
	\paragraph{Seeding the random number generator} ~\\
	At the beginning of main, seed the random number generator like this:
	
\begin{lstlisting}[style=code]
srand( time( NULL ) );
\end{lstlisting}

	You will need to make sure to \#include \texttt{cstdlib} (for rand() and srand())
	and \texttt{time} (for time()). If you put a number in the seed, like
	\texttt{srand( 10 );}, then every time you run the program you would get the
	\textit{same} string of random numbers in the same order. Seeding it with
	\texttt{time( NULL )} gives us the date/time, and since we usually won't
	run the program twice in any given second, it will usually give us
	a good enough seed.
	
	
	\newpage
	\subsection*{Class 2: Students}
	For this program, we will create a class for Student information.
	Our student object will have some basic information:
	
	\begin{center}
		\begin{tabular}{ | l | }
			\hline
			\multicolumn{1}{|c|}{\textbf{Student}}
			\\ \hline
			- fullName : string \\
			- grades : float[10] \\ 
			- totalGrades : int \\
			\hline
			+ Student() : constructor \\
			+ Setup( newName : string ) : void \\ 
			+ AddGrade( float score ) : void \\
			+ GetGpa() : float \\
			+ GetName() : string \\
			+ Display() : void \\
			\hline
		\end{tabular}
	\end{center}
	
\begin{lstlisting}[style=code]
#ifndef _STUDENT_HPP
#define _STUDENT_HPP

#include <string>
using namespace std;

class Student
{
	public:
	Student();
	
	void Setup( string newName );
	void AddGrade( float score );
	float GetGpa();
	string GetName();
	void Display();
	
	private:
	string fullName;
	float grades[10];
	int totalGrades;
};

#endif
\end{lstlisting}
	
	\newpage
	\paragraph{Constructor:} The constructor will initialize the \texttt{totalGrades} count to 0.
	We will use this variable as we work with the array of \texttt{grades} for the student.
	
\begin{lstlisting}[style=code]
Student::Student()
{
	totalGrades = 0;
}
\end{lstlisting}

	\paragraph{Setup:} Initialize the student's name.
	
\begin{lstlisting}[style=code]
void Student::Setup( string newName )
{
	fullName = newName;
}
\end{lstlisting}

	\paragraph{AddGrade:} Add another grade to their list of grades. This should be between 0.0 and 4.0;
	don't allow anything outside of that range.
	You will also have to check if the array is full.

\begin{lstlisting}[style=code]
void Student::AddGrade( float score )
{
	if ( score < 0 || score > 4 )
	{
		cout << "Score is out of range!" << endl;
		return; // leave the function
	}
	
	if ( totalGrades == 10 )
	{
		cout << "Grades array is full!" << endl;
		return; // leave the function
	}
	
	grades[ totalGrades ] = score;
	totalGrades++;
}
\end{lstlisting}

	\newpage
	\paragraph{GetGpa:}	Calculates the GPA and returns it.
	
\begin{lstlisting}[style=code]
float Student::GetGpa()
{
	float total = 0;
	for ( int i = 0; i < totalGrades; i++ )
	{
		total += grades[i];
	}
	return total / totalGrades;
}
\end{lstlisting}
	
	\paragraph{GetName:} Return the student's name.
	
\begin{lstlisting}[style=code]
string Student::GetName()
{
	return fullName;
}
\end{lstlisting}

	\paragraph{Display:} Display the student's name and GPA. We
	can call the \texttt{GetGpa()} and \texttt{GetName()} functions here.
	
\begin{lstlisting}[style=code]
void Student::Display()
{
	cout << GetName() << ": " << GetGpa() << endl;
}
\end{lstlisting}

	\paragraph{Example program:} Implement a program in main that 
	lets the user enter student information and then displays the
	student's GPA once done.
	
	You could also create an array of students and set up each one,
	then display a list of all the students and their GPAs.

	\newpage
	\subsection*{Class 3: Cards and Deck}
	For this program, we will create a \textbf{Card} class and
	create an array of cards that is stored in a \textbf{Deck} class in order to build an entire deck of 52.
	
	\deckofcards
	
	\subsubsection*{Card class} 
	
	Create a Card.hpp file (and Card.cpp file if you want),
	and declare a class based on this UML diagram. The top section shows
	\textbf{member variables}, with the ``-'' sign meaning \textbf{private},
	and the bottom section shows \textbf{member functions}, with the ``+'' sign
	meaning \textbf{public}.
	
	\begin{center}
		\begin{tabular}{ | l | }
			\hline
			\multicolumn{1}{|c|}{\textbf{Card}}
			\\ \hline
			- m\_rank : string \\
			- m\_suit : char \\ \hline
			+ Setup( newRank : string, newSuit : char ) : void \\ 
			+ Display() : void \\
			+ DisplayName() : void \\
			+ GetRank() : string \\
			+ GetSuit() : char \\
			\hline
		\end{tabular}
	\end{center}
	
	\begin{itemize}
		\item	\textbf{Setup:} Use the setup function to initialize
				the member variables \texttt{m\_rank} and \texttt{m\_suit},
				setting them to whatever values are passed in as the parameters.
				
		\item	\textbf{Display:} Display (with \texttt{cout}) the rank and suit together in a concise format, e.g.: A-H for Ace of Hearts.
		
		\item	\textbf{DisplayName:} Display (with \texttt{cout}) the rank and suit together written out to be readable, e.g.: Ace of Hearts.
		
		\item	\textbf{GetRank:} Return the value of \texttt{m\_rank}.
		\item	\textbf{GetSuit:} Return the value of \texttt{m\_suit}.
	\end{itemize}
	
	\newpage
	\subsubsection*{Deck class}
	
	The Deck class is going to store the array of all cards. We will use a 2D array
	with one dimension being the SUIT, and one dimension being the RANK.
	
	\begin{center}
		\begin{tabular}{ | l | }
			\hline
			\multicolumn{1}{|c|}{\textbf{Deck}}
			\\ \hline
			- m\_cards : Card[4][13] \\
			+ Deck() \\
			+ CreateDeck() : void \\ 
			+ DisplayCard( int s, int r ) : void 
			\\ 
			\hline
		\end{tabular}
	\end{center}
	
	\begin{itemize}
		\item	\textbf{Constructor:}	The Deck() constructor should just call the \texttt{CreateDeck()} function.
		\item	\textbf{CreateDeck:}	This function should build the deck. First in the function, let's create some helpers:
		
\begin{lstlisting}[style=code]
const int SUITS = 4;
const int RANKS = 13;

const char suits[] = { 'C', 'D', 'H', 'S' };
const string ranks[] = { "A", "1", "2", "3", "4", 
						"5", "6", "7", "8", "9", 
						"10", "J", "Q", "K" };
\end{lstlisting} 	~\\
		These variables/arrays just make it easier to get ``names'' of each suit and rank
		as we're setting up the cards in the deck. 		
		You can then initialize all the cards with a nested for-loop: one loop for suits, one loop for ranks.

\begin{lstlisting}[style=code]
for ( int s = 0; s < SUITS; s++ )
{
	for ( int r = 0; r < RANKS; r++ )
	{
		// Getting C, D, H, or S from the array
		char suit = suits[ s ];
		
		// Getting rank name from the array
		string rank = ranks[ r ];
		
		// Setting up the card
		m_cards[s][r].Setup( suit, rank );
	}
}
\end{lstlisting}

		Once the setup is done, the Deck class will contain all 52 cards.
		
		\item \textbf{DisplayCard:}	This takes in two indices, \texttt{s} and \texttt{r}, which correspond
			to some card in the deck. For this function, access one of \texttt{m\_cards[s][r]}'s Display() functions
			to write that card's information to the screen.
			
\begin{lstlisting}[style=code]
cout << "Card [" << s << "][" << r << "] is: ";
m_cards[s][r].DisplayName();
cout << endl;
\end{lstlisting}
	\end{itemize}
	
	\subsubsection*{Example card program}
		Back in \textbf{main()}, for program 3 you will want to declare a Deck object:
		
\begin{lstlisting}[style=code]
Deck myDeck;
\end{lstlisting}

		The Deck's constructor will automatically have all the cards generated.
		Next in the program, you can ask the user to select a number for suit (0 to 3)
		and a number for rank (0 to 12) and then display which card corresponds to that,
		or you could have the computer choose at random.
		
\begin{lstlisting}[style=code]
int suit, rank;

// User chosen
cout << "Enter a suit (0 - 3): ";
cin >> suit;

cout << "Enter a rank (0 - 12): ";
cin >> rank;

myDeck.DisplayCard( suit, rank );

// Computer chosen
suit = rand() % 4;
rank = rand() % 12;

myDeck.DisplayCard( suit, rank );
\end{lstlisting}

	\newpage
	\subsection*{Class 4: Ingredients and Recipe}

	For this one, we will have two classes: Ingredients and Recipe. A recipe
	will contain an array of ingredients, and one ingredient will store the
	ingredient name, the measurement amount, and what kind of measurement to use.
	
	\paragraph{Ingredient:}
	
	\begin{center}
		\begin{tabular}{ | l | }
			\hline
			\multicolumn{1}{|c|}{\textbf{Ingredient}}
			\\ \hline
			- m\_name : string \\
			- m\_unit : string \\ 
			- m\_amount : float \\
			\hline
			+ Setup( name : string, amount : float, unit : string ) : void \\ 
			+ Display() : void \\
			\hline
		\end{tabular}
	\end{center}
	
	Use the \textbf{Setup} function to set up the ingredient's member variables,
	and use \textbf{Display} to write the ingredient, amount, and unit to the screen
	in a nice format.
	
	
\begin{lstlisting}[style=code]
#ifndef _INGREDIENT_HPP
#define _INGREDIENT_HPP

#include <string>
using namespace std;

class Ingredient
{
  public:
  void Setup( string name, float amount, string unit );
  void Display();

  private:
  string m_name;
  string m_unit;
  float m_amount;
};

#endif
\end{lstlisting}

	\newpage
	\paragraph{Recipe:}

	\begin{center}
		\begin{tabular}{ | l | }
			\hline
			\multicolumn{1}{|c|}{\textbf{Recipe}}
			\\ \hline
			- m\_name : string \\
			- m\_instructions : string \\
			- m\_ingredients : Ingredient[10] \\ 
			- m\_totalIngredients : int \\
			\hline
			+ Recipe() : constructor \\
			+ SetName( name : string ) \\
			+ SetInstructions( instructions : string ) \\
			+ AddIngredient( name : string, amount : float, unit : string ) : void \\ 
			+ Display() : void \\
			\hline
		\end{tabular}
	\end{center}
	
		\subparagraph{Constructor:} Initialize the \texttt{m\_totalIngredients} to 0.
		\subparagraph{SetName:} Set the recipe's \texttt{m\_name}.
		\subparagraph{SetInstructions:} Set the recipe's \texttt{m\_instructions}.
		\subparagraph{AddIngredient:} Call the ingredient's \texttt{Setup()} function,
				passing in the ingredient information. Make sure to validate if the
				array is full and don't add anything else if it's full.
				
\begin{lstlisting}[style=code]
void Recipe::AddIngredient( string name, float amount, string unit )
{
	if ( m_totalIngredients == 10 )
	{
		cout << "Ingredient list is full!" << endl;
		return;
	}
	
	m_ingredients[ m_totalIngredients ].Setup( name, 
												amount, 
												unit );
	m_totalIngredients++;
}
\end{lstlisting}
						
		\newpage
		\subparagraph{Display:} Use a for loop to iterate over all the ingredients,
				calling \texttt{Display} on each ingredient to list it out.


\begin{lstlisting}[style=code]
void Recipe::Display()
{
	cout << m_name << endl;
	
	for ( int i = 0; i < m_totalIngredients; i++ )
	{
		m_ingredients.Display();
	}
	
	cout << m_instructions << endl;
}
\end{lstlisting}

	\paragraph{Example program:}
	For an example program, you can create a \texttt{Recipe} object
	and use the \texttt{AddIngredient} function to add a bunch of ingredients,
	then use the \texttt{Display} function to view the finished recipe.

\begin{lstlisting}[style=code]
Recipe cookies;

cookies.SetName( "Chocolate Chip Cookies" );

cookies.AddIngredient( "Butter", 1, "cup" );
cookies.AddIngredient( "White sugar", 1, "cup" );
cookies.AddIngredient( "Eggs", 2, "items" );
cookies.AddIngredient( "Vanilla", 2, "teaspoons" );
cookies.AddIngredient( "Baking soda", 1, "teaspoon" );
// and so on

cookies.SetInstructions( "Preheat oven to 350 F, combine ingredients in bowl, put on cookie sheet, bake for 10 minutes." );

cookies.Display();
\end{lstlisting}

	

\end{document}
