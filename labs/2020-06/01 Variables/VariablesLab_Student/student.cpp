#include <iostream>
#include <string>
using namespace std;

int main()
{
    // 1. Declare and initialize variables
    string email = "student@jccc.edu";
    char grade = 'A';
    float gpa = 3.98;
    int semestersCompleted = 2;
    bool degreeSeeking = true;

    // 2. Display student information
    cout << "STUDENT INFORMATION" << endl;
    cout << "Email:                 " << email << endl;
    cout << "Grade:                 " << grade << endl;
    cout << "GPA:                   " << gpa << endl;
    cout << "Semesters completed:   " << semestersCompleted << endl;
    cout << "Degree seeking?:       " << degreeSeeking << endl;

    // 3. Have user update info
    cout << endl << "UPDATE STUDENT INFO" << endl;

    cout << "Enter email: ";
    cin >> email;

    cout << "Enter grade (A, B, C, D, or F): ";
    cin >> grade;

    cout << "Enter gpa: ";
    cin >> gpa;

    cout << "Enter semesters completed: ";
    cin >> semestersCompleted;

    cout << "Enter degree seeking (1 = yes, 0 = no): ";
    cin >> degreeSeeking;

    // 4. Display updated info
    cout << endl << "UPDATED STUDENT INFORMATION" << endl;
    cout << "Email:               " << email << endl;
    cout << "Grade:               " << grade << endl;
    cout << "GPA:                 " << gpa << endl;
    cout << "Semesters completed: " << semestersCompleted << endl;
    cout << "Degree seeking?:     " << degreeSeeking << endl;

    return 0;
}
