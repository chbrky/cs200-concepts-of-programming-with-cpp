#include <iostream>
using namespace std;

#include "MovieLibrary.hpp"

void DisplayMainMenu( int savedMovies );
int GetChoice( int min, int max );

int main()
{
    MovieLibrary movieLibrary;

    bool done = false;
    while ( !done )
    {
        DisplayMainMenu( movieLibrary.GetMovieCount() );
        int choice = GetChoice( 1, 5 );
        cout << endl;

        switch( choice )
        {
            case 1: { // add movie
                cout << "Enter movie title: ";
                string title;
                cin.ignore();
                getline( cin, title );
                movieLibrary.AddMovie( title );
            } break;

            case 2: { // update movie
                movieLibrary.ViewAllMovies();
                cout << "Update which movie #? ";
                int index;
                cin >> index;

                cout << "Enter movie title: ";
                string title;
                cin.ignore();
                getline( cin, title );

                movieLibrary.UpdateMovie( index, title );
            } break;

            case 3: // clear all movies
                movieLibrary.ClearAllMovies();
            break;

            case 4: // view all movies
                movieLibrary.ViewAllMovies();
            break;

            case 5: // quit
            done = true;
            break;
        }
    }

    return 0;
}


void DisplayMainMenu( int savedMovies )
{
    cout << endl << "---------------------------------" << endl;
    cout << "MOVIE COLLECTION" << endl
         << savedMovies << " total movie(s)" << endl << endl;
    cout << "MAIN MENU" << endl
         << "---------" << endl
         << "1. Add movie" << endl
         << "2. Update movie" << endl
         << "3. Clear all movies" << endl
         << "4. View all movies" << endl
         << "5. Save and quit" << endl;
}

int GetChoice( int min, int max )
{
    int choice;
    cout << "(" << min << " - " << max << ") >> ";
    cin >> choice;

    while ( choice < min || choice > max )
    {
        cout << "Invalid choice, try again." << endl;
        cout << ">> ";
        cin >> choice;
    }

    return choice;
}
