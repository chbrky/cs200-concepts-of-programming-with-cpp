\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {Searching and Sorting}
\newcommand{\laTitle}       {CS 200 Lab}
\newcounter{question}

\renewcommand{\chaptername}{Topic}

\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}

\title{}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ / \laTitle}

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

\section*{Lab instructions}

\paragraph{Turn in:}

For this lab, you will turn in a text document with your logs of
how long each algorithm runs.

\section*{\laTopic\ lab}

	\subsection*{Starter code:}
	
	For this lab, all the code is already written, but I want you to investigate
	the time it takes to search and sort for data given a large data set.
	On Canvas, you will need to download the starter code pack, which contains:
	
	\begin{itemize}
		\item	animal.hpp, animal.cpp
		\item	main.cpp
		\item	menu.hpp
		\item	parser.hpp, parser.cpp
		\item	search.hpp, search.cpp
		\item	sort.hpp, sort.cpp
		\item	timer.hpp
		\item	data/Animal\_Services\_Intake\_Data.csv
		\item	data/data source.txt
	\end{itemize}
	
	You will need to create a new project and then move all these files
	into your project directory. The .csv file should remain in a data folder
	inside your project directory, otherwise you'll have to update 
	the argument for the \texttt{LoadCsvData} function call in main().
	
	\newpage
	\section*{About the program:}
	
	This program loads in a large data set downloaded from data.gov.
	This file has 187,594 total records in it, and so doing any searching
	or sorting will take a noticable length of time.
	
	\begin{center}
		\includegraphics[width=14cm]{images/datacsv.png}
		~\\
		The data opened in LibreOffice
	\end{center}
	\begin{center}
		\includegraphics[width=14cm]{images/dataplaintext.png}
		~\\
		The data opened in a text editor
	\end{center}
	
	This file type is a \textbf{comma-separated value file}, or .csv file.
	It is basically a spreadsheet with columns separated by commas and rows
	separated by newlines.
	
	\newpage
	\section*{About the code:}
	
	All of the code has already been written, but here's some information
	about it if you want to explore it.
	
	\paragraph{main.cpp:} Contains the main() function and basic menus to
	let the user choose to search or sort the data set.
	
	\paragraph{search.hpp and search.cpp:} Contains function declarations
	and definitions for Linear Search and Binary Search.
	
	\paragraph{sort.hpp and sort.cpp:} Contains function declarations
	and definitions for Insertion Sort, Selection Sort, and Merge Sort.
	
	\paragraph{parser.hpp and parser.cpp:} Contains the code used to
	read in the .csv text file using an \texttt{ifstream}.
	
	\paragraph{animal.hpp and animal.cpp:} Contains a class called \textbf{Animal},
	which contains variables for the different fields in each record (animal ID, type,
	breed, etc.)
	
	\paragraph{menu.hpp:} My menu building library so I don't have to rewrite
	the same functionality over and over. :)
	
	\paragraph{timer.hpp:} My timer library to keep track of ellapsed milliseconds.
	
	\newpage
	\section*{Instructions}
	When you run the program, it will begin my loading in the data file.
	It will give you an error if it can't find the data file, so make
	sure it is loading properly before working on the lab.
	
\begin{lstlisting}[style=output]
Loading file "data/Animal_Services_Intake_Data.csv...
* Done loading file (451 milliseconds)
* 187593 records loaded
--------------------------------------------------------
 | Main Menu |
 -------------

 1.	Sort animal IDs
 2.	Search for animal by ID

 >> 

\end{lstlisting}

	The first option lets you sort the data loaded in. This will sort
	the animals by their unique Animal ID\#. When you select
	the first option, it will ask which sorting algorithm to use.

\begin{lstlisting}[style=output]
--------------------------------------------------------
 | Sort animal IDs |
 -------------------

 1.	Insertion sort
 2.	Selection sort
 3.	Merge sort

 >> 

\end{lstlisting}

	\newpage
	Insertion and Selection sort are algorithms that are pretty easy to
	read, but are not very efficient. Merge sort is more efficient,
	but is a more complex algorithm.


	\begin{center}
		\begin{tabular}{l | c c c}
			\textbf{Algorithm} 	& \textbf{Best} & \textbf{Average} & \textbf{Worst}  \\
			Insertion sort		& 	$O(n)$ & $O(n^2)$ & $O(n^2)$  \\
			Selection sort 		&	$O(n^2)$ & $O(n^2)$ & $O(n^2)$  \\
			Merge sort			& 	$O(n \cdot log(n))$ & $O(n \cdot log(n))$ & $O(n^2)$
		\end{tabular}
	\end{center}


	\begin{center}
		\begin{tikzpicture}
		  \begin{axis}[grid=major,xmin=0,xmax=5,ymax=5,xlabel={Amount of data $n$},ylabel={Time complexity $O(log(n))$}]
			\addplot[colorblind_medium_blue,ultra thick] {x * log10(x)};
			\addplot[colorblind_medium_orange,ultra thick,dashed] {x^2};
		  \end{axis}
		  \node at (1,2) {$n^2$};
		  \node at (5,2) {$n \cdot log(n)$};
		\end{tikzpicture}
	\end{center}
	
	\paragraph{Log 1:} Run each sort \textbf{after closing and reopening the program} (so that the data is not sorted again)
	and keep track of the different sort times. Some may take a few minutes, so be patient.
	Store your findings in a text file.
	
	~\\ For example, on my machine:
	\begin{center}
		\begin{tabular}{l p{6cm}}
			\textbf{Sorting algorithm} 	& \textbf{Milliseconds to complete} \\ \hline
			Insertion sort 				& 116932 milliseconds 
			
										($\approx$ 116 seconds)
			\\ \hline
			Selection sort 				& 488265 milliseconds 
			
										($\approx$ 488 seconds, 8 minutes)
			\\ \hline
			Merge sort 					& 1191 milliseconds
		\end{tabular}
	\end{center}
	~\\
	Your results will be different since it depends on how fast your machine is.


	\newpage
	For the second option, you can search for an animal by ID. You can also
	open up the \textbf{Animal\_Services\_Intake\_Data.csv} file in Excel
	to get a look at the data.
		
	After you enter an ID, it will ask what searching algorithm to use.

\begin{lstlisting}[style=output]
--------------------------------------------------------
 | Search for animal by ID |
 ---------------------------

 Enter the Animal ID

 >> A0203801

 1.	Linear search
 2.	Binary search

 >> 
\end{lstlisting}

	You can use the Linear Search on unsorted data, but the 
	Binary Search \underline{will not work} until the data is \textbf{sorted}
	so make sure you've already run one of the sorts before using Binary Search.  ~\\
	
	Running the Linear Search, it will search for the ID from the
	beginning of the array to the end and return the index where it was
	found, or -1 if not found. It will then display the animal's information.
	
\begin{lstlisting}[style=output]
Beginning Linear Search...
* Search completed with index 47 (41 milliseconds)

Animal ID:         A0203801
Shelter:           E VALLEY
Intake date:       10/23/2011
Intake type:       OWNER SUR
Intake condition:  ALIVE
Animal type:       DOG
Group:             TERRIER
Breed:             TERRIER MIX-
\end{lstlisting}
	
	The searches will run pretty quickly relative to the sorts.
	
	\newpage
	\paragraph{Log 2:} Try doing some searches in IDs from different points
	in the data file (at the beginning, at the end, etc.) and compare
	the Linear Search to the Binary Search.
	

	~\\ For example:
	\begin{center}
		\begin{tabular}{p{4cm} l p{4cm}}
			\textbf{ID...} 				& \textbf{Searching algorithm}	& \textbf{Completion time} \\ \hline
			A0191772
			
			Near beginning of file		& Linear search 				& 43 milliseconds
			\\
										& Binary search					& 43 milliseconds
			\\ \hline
			A0268713
			
			Near end of file			& Linear search					& 60 milliseconds
			\\
										& Binary search					& 56 milliseconds
		\end{tabular}
	\end{center}
	
	Select four animal IDs to search on in different parts of the file - 
	at the beginning, at the end, half-way through, three-fourths-way through, etc.
	Search for the same ID with both Linear Search and Binary Search and write down
	their completion times.
	
\end{document}
