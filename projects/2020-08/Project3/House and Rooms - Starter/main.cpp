#include "House/House.hpp"
#include "Utilities/Menu.hpp"

int main()
{
    House myHouse;

    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );
        cout << myHouse.GetRoomCount() << " total room(s)" << endl << endl;

        string choice = Menu::ShowStringMenuWithPrompt( {
            "Add room",
            "Update room",
            "Display house info",
            "Save and quit"
        } );

        if ( choice == "Add room" )
        {
            myHouse.AddRoom();
        }
        else if ( choice == "Update room" )
        {
            myHouse.UpdateRoom();
        }
        else if ( choice == "Display house info" )
        {
            myHouse.Display();
        }
        else if ( choice == "Save and quit" )
        {
            done = true;
        }
    }
}
