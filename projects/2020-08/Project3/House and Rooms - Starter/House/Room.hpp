#ifndef _ROOM_HPP
#define _ROOM_HPP

#include <string>
using namespace std;

class Room
{
    public:
    void Setup( string name, float width, float length, float height );
    void UserSetup();

    float GetSquareFootage();
    string GetName();
    float GetWidth();
    float GetLength();
    float GetHeight();

    void Display();

    private:
    float m_width;
    float m_length;
    float m_height;
    string m_name;
};

#endif
