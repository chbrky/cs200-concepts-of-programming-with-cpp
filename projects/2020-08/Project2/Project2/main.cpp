#include <iostream>
#include <string>
using namespace std;

#include "storage.hpp"
#include "menus.hpp"

#include "cutest/StorageTester.hpp"
#include "utilities/Logger.hpp"

int main()
{
    Logger::Setup();
    /******************************************************* TESTER */
    // Run automated tests
    StorageTester tester;
    tester.Start();

    /******************************************************* MAIN PROGRAM */
    // Initialization
    const int   ARRAY_SIZE = 10;    // Size of the array
    string      arr[ARRAY_SIZE];    // Stores all the data
    int         itemCount = 0;      // How many items we're storing right now

    // Try to load the save file if possible.
    // * FUNCTION:  Call the LoadItems function to populate the array.
    LoadItems( "save.txt", arr, itemCount );

    // Helper variables
    string  tempName;
    int     tempIndex;

    // Run program
    bool done = false;
    while ( !done )
    {
        ClearScreen();
        DisplayMainMenu( itemCount );
        int choice = GetChoice( 1, 5 );
        cin.ignore();

        switch( choice )
        {
            case 1: // Add item
            {
                cout << endl << "ADD ITEM..." << endl;
                // * If the array is full, then we can't add a new item.
                //   Display an error message.
                // * Otherwise, we can add a new item:
                //   * PROMPT:     Ask the user to enter a name.
                //   * INPUT:      Get the user's input and store it in tempName.
                //   * FUNCTION:   Use the AddItem function to add it to the array.
                if ( IsFull( itemCount, ARRAY_SIZE ) )
                {
                    cout << "Can't add item! Array is full!" << endl;
                }
                else
                {
                    cout << "Enter new item: ";
                    getline( cin, tempName );
                    AddItem( tempName, arr, itemCount, ARRAY_SIZE );
                }
            }
            break;

            case 2: // Update items
            {
                cout << endl << "UPDATE ITEM..." << endl;
                // * PROMPT:    Ask the user to enter the index of the item they want to modify.
                // * INPUT:     Get the user's input and store it in tempIndex.
                // * PROMPT:    Ask the user what they want to replace that item with.
                // * INPUT:     Get the user's input and store it in tempName.
                // * FUNCTION:  Call the UpdateItem function to update the item.
                cout << "Enter item index: ";
                cin >> tempIndex;
                cin.ignore();
                cout << "Enter updated item: ";
                getline( cin, tempName );
                UpdateItem( tempIndex, tempName, arr, itemCount, ARRAY_SIZE );
            }
            break;

            case 3: // Clear all items
            {
                cout << endl << "CLEAR ALL ITEMS..." << endl;
                // * FUNCTION:  Call the ClearAllItems function to clear out the array.
                ClearAllItems( arr, itemCount, ARRAY_SIZE );
            }
            break;

            case 4: // View all items
            {
                cout << endl << "ALL ITEMS..." << endl;
                // * FUNCTION:  Call the ViewAllItems function to display all items to the screen.
                ViewAllItems( arr, itemCount );
            }
            break;

            case 5: // Save and quit
            {
                cout << endl << "SAVE AND QUIT..." << endl;
                // * FUNCTION:  Call the SaveItems function to save it to a text file.
                // * Afterward, set done to true.
                SaveItems( "save.txt", arr, itemCount );
                done = true;
            }
            break;
        }

        // Pause before the loop re-runs
        Pause();
    }

    Logger::Cleanup();
    return 0;
}
