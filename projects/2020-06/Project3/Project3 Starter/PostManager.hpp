#ifndef _POST_MANAGER_HPP
#define _POST_MANAGER_HPP

#include "Post.hpp"

#include <string>
#include <vector>
using namespace std;

class PostManager
{
    public:
    PostManager();
    ~PostManager();

    void CreateNewPost( string username );
    void PrintAllPosts() const;

    private:
    vector<Post> m_posts;

    void Load();
    void Save();
};

#endif
