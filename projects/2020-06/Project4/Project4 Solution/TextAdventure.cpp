#include "TextAdventure.hpp"

#include <iostream>
#include <fstream>
using namespace std;

TextAdventure::TextAdventure()
{
    m_isDone = false;
    m_rooms = nullptr;
    m_ptrCurrentRoom = nullptr;
    LoadGameData();
}

TextAdventure::~TextAdventure()
{
    DeallocateSpace();
}

void TextAdventure::Run()
{
    string command;
    while ( !m_isDone )
    {
        m_ptrCurrentRoom->Display();
        command = GetUserCommand();

        if ( command == "quit" || command == "q" )
        {
            m_isDone = true;
        }
        else
        {
            TryToMove( command );
        }
    }
}

void TextAdventure::TryToMove( string command )
{
    cout << endl;
    if ( command == "west" || command == "w" )
    {
        if ( m_ptrCurrentRoom->m_ptrNeighborWest == nullptr )
        {
            cout << "Cannot move west!" << endl;
        }
        else
        {
            cout << "You moved west." << endl;
            m_ptrCurrentRoom = m_ptrCurrentRoom->m_ptrNeighborWest;
        }
    }
    else if ( command == "east" || command == "e" )
    {
        if ( m_ptrCurrentRoom->m_ptrNeighborEast == nullptr )
        {
            cout << "Cannot move east!" << endl;
        }
        else
        {
            cout << "You moved east." << endl;
            m_ptrCurrentRoom = m_ptrCurrentRoom->m_ptrNeighborEast;
        }
    }
    else if ( command == "north" || command == "n" )
    {
        if ( m_ptrCurrentRoom->m_ptrNeighborNorth == nullptr )
        {
            cout << "Cannot move north!" << endl;
        }
        else
        {
            cout << "You moved north." << endl;
            m_ptrCurrentRoom = m_ptrCurrentRoom->m_ptrNeighborNorth;
        }
    }
    else if ( command == "south" || command == "s" )
    {
        if ( m_ptrCurrentRoom->m_ptrNeighborSouth == nullptr )
        {
            cout << "Cannot move south!" << endl;
        }
        else
        {
            cout << "You moved south." << endl;
            m_ptrCurrentRoom = m_ptrCurrentRoom->m_ptrNeighborSouth;
        }
    }
    else
    {
        cout << "Unknown command!" << endl;
    }
    cout << endl;
}

string TextAdventure::GetUserCommand()
{
    cout << endl << endl;
    cout << "Move commands: north / south / east / west" << endl;
    cout << "Game commands: quit" << endl;
    cout << endl;

    string command;
    cout << "Command: ";
    cin >> command;

    // make it lower-case
    for ( unsigned int i = 0; i < command.size(); i++ )
    {
        command[i] = tolower( command[i] );
    }

    return command;
}

void TextAdventure::LoadGameData()
{
    ifstream input( "data/rooms.txt" );
    string buffer;
    int totalRooms;
    int roomCounter = 0;

    input >> buffer;    // TOTAL_ROOMS
    input >> totalRooms;

    AllocateSpace( totalRooms );

    while ( getline( input, buffer ) )
    {
        if ( buffer == "NAME" )
        {
            getline( input, m_rooms[ roomCounter ].m_name );
        }
        else if ( buffer == "DESCRIPTION" )
        {
            getline( input, m_rooms[ roomCounter ].m_description );
        }
        else if ( buffer == "ROOM_END" )
        {
            roomCounter++;
        }
        else if ( buffer == "NEIGHBORS" )
        {
            string roomName1, roomName2;
            int roomIndex1, roomIndex2;
            string directionString;

            // Room name
            getline( input, roomName1 );
            getline( input, directionString );
            getline( input, roomName2 );

            roomIndex1 = GetIndexOfRoomWithName( roomName1 );
            roomIndex2 = GetIndexOfRoomWithName( roomName2 );

            if ( directionString == "WEST" )
            {
                m_rooms[ roomIndex1 ].m_ptrNeighborWest = &m_rooms[ roomIndex2 ];
                m_rooms[ roomIndex2 ].m_ptrNeighborEast = &m_rooms[ roomIndex1 ];
            }
            else if ( directionString == "EAST" )
            {
                m_rooms[ roomIndex1 ].m_ptrNeighborEast = &m_rooms[ roomIndex2 ];
                m_rooms[ roomIndex2 ].m_ptrNeighborWest = &m_rooms[ roomIndex1 ];
            }
            else if ( directionString == "NORTH" )
            {
                m_rooms[ roomIndex1 ].m_ptrNeighborNorth = &m_rooms[ roomIndex2 ];
                m_rooms[ roomIndex2 ].m_ptrNeighborSouth = &m_rooms[ roomIndex1 ];
            }
            else if ( directionString == "SOUTH" )
            {
                m_rooms[ roomIndex1 ].m_ptrNeighborSouth = &m_rooms[ roomIndex2 ];
                m_rooms[ roomIndex2 ].m_ptrNeighborNorth = &m_rooms[ roomIndex1 ];
            }
        }
    }

    // Set the first room to room #0
    m_ptrCurrentRoom = &m_rooms[0];
}

void TextAdventure::AllocateSpace( int size )
{
    DeallocateSpace();

    m_rooms = new Room[ size ];
    m_totalRooms = size;
}

void TextAdventure::DeallocateSpace()
{
    if ( m_rooms != nullptr )
    {
        delete [] m_rooms;
        m_totalRooms = 0;
        m_rooms = nullptr;
    }
}

int TextAdventure::GetIndexOfRoomWithName( string name )
{
    for ( int i = 0; i < m_totalRooms; i++ )
    {
        if ( m_rooms[i].m_name == name )
        {
            return i;
        }
    }
    return -1;
}
