import java.util.Scanner;

public class YorN
{
    static Scanner input = new Scanner( System.in );

    public static String GetYOrN()
    {
        System.out.print( "(y/n): " );
        String choice = input.next();

        while ( choice.equals( "y" ) == false && choice.equals( "n" ) == false )
        {
            System.out.println( "Invalid input. Try again" );
            System.out.print( "(y/n): " );
            choice = input.next();
        }

        return choice;
    }
    
    public static void main( String args[] )
    {
        boolean quit = false;

        while ( quit == false )
        {
            System.out.println( "Quit?" ) ;
            String choice = GetYOrN();

            if ( choice.equals( "y" ) )
            {
                quit = true;
            }
        }
    }
}
