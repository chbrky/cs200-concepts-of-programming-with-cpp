import java.util.Scanner;

public class ValidateNumbers
{
    static Scanner input = new Scanner( System.in );

    public static int GetChoice( int min, int max )
    {
        int choice;
        System.out.print( "Please enter a number between " + min + " and " + max + ": " );
        choice = input.nextInt();

        while ( choice < min || choice > max )
        {
            System.out.println( "Invalid input, try again: " );
            choice = input.nextInt();
        }
        return choice;
    }
    
    public static void main( String args[] )
    {
        int choice = GetChoice( 1, 5 );

        System.out.println( "Choice was " + choice );
    }

    public static boolean RunAgain()
    {
        System.out.print( "Run again? (y/n): " );
        String choice = input.next();

        if ( choice.equals( "n" ) )
        {
            return false;
        }
        return true;
    }
}
