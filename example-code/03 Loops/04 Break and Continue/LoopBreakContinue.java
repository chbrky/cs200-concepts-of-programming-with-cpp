import java.util.Scanner;

public class LoopBreakContinue
{
    public static void main( String[] args )
    {
        int i = 0;

        while ( true )
        {
                i++;
                if ( i % 5 == 0 )
                {
                        System.out.println();
                        continue;
                }

                if ( i > 100 )
                {
                        break;
                }
                
                System.out.print( i + " " );
        }
    }
}
