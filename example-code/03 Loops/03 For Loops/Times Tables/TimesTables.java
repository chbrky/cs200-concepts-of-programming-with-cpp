import java.util.Scanner;

public class TimesTables
{
    public static void main( String[] args )
    {
        Scanner input = new Scanner( System.in );
        
        int max;
        System.out.print( "Times tables from 1 to: " );
        max = input.nextInt();

        System.out.println();

        for ( int i = 1; i <= max; i++ )
        {
                for ( int j = 1; j <= max; j++ )
                {
                        System.out.print( i*j + "\t" );
                }
                System.out.println();
        }
        
    }
}
